#!/bin/bash

if [[ $OSTYPE == linux* || $OSTYPE == darwin* || $OSTYPE == bsd* ]]; then
     binDir=$(cd -P -- "$(dirname -- "$0")" && pwd -P)/src-sx/bin/
     cd $binDir
     
     ./cake crontasks
fi;

if [[ $OSTYPE == msys* ]]; then
    .\src-sx\bin\cake crontasks
fi;
