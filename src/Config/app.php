<?php

return [
    'Datasources' => [
        'default' => [
            'host' => env('MYSQL_HOST', 'localhost'),
            'username' => env('MYSQL_USER', 'root'),
            'password' => env('MYSQL_PASS', ''),
            'database' => env('MYSQL_NAME', ''),
            'port' => env('MYSQL_PORT', '3306'),
            'log' => filter_var(env('MYSQL_LOG', false), FILTER_VALIDATE_BOOLEAN),
        ],

        'test' => [
            'host' => env('MYSQL_TEST_HOST', 'localhost'),
            'username' => env('MYSQL_TEST_USER', 'root'),
            'password' => env('MYSQL_TEST_PASS', ''),
            'database' => env('MYSQL_TEST_NAME', ''),
            'port' => env('MYSQL_TEST_PORT', '3306'),
            'log' => filter_var(env('MYSQL_TEST_LOG', true), FILTER_VALIDATE_BOOLEAN),
        ],
    ],

    'Elasticsearch' => [
        'config' => [
            'host' => '9da97c3d02e641ada8f159de10050c48.eu-central-1.aws.cloud.es.io',
            'port' => '9243',
            'scheme' => 'https',
            'user' => 'elastic',
            'pass' => 'qsYvF97pNAglmVOhFI5NcrTZ',
        ],
        'indexprefix' => env('ES_INDEXPREFIX', 'myprefix'),
    ],

];
