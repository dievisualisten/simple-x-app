<?php

return [
    'Media' => [
        'whiteSpaceAndBorderColor' => '#fff', //RGB WERTE
        'formats' => [
            '4_3' => [
                'ratio_spacer' => '4_3',
                'info' => [
                    'name' => 'Format 4:3',
                    'description' => '',
                ],
                'size' => [
                    'w' => 1920,
                    'h' => 1440,
                ],
                'transform' => [
                    'type' => 'scalecrop',
                    'allowupscale' => false,
                ],
                'imagine' => [
                    'jpeg_quality' => 100,
                    'png_compression_level' => 7,
                ],
            ],
            '2_1' => [
                'ratio_spacer' => '2_1',
                'info' => [
                    'name' => 'Format 2:1',
                    'description' => '',
                ],
                'size' => [
                    'w' => 1920,
                    'h' => 960,
                ],
                'transform' => [
                    'type' => 'scalecrop',
                    'allowupscale' => false,
                ],
                'imagine' => [
                    'jpeg_quality' => 100,
                    'png_compression_level' => 7,
                ],
            ],
            '1_2' => [
                'ratio_spacer' => '1_2',
                'info' => [
                    'name' => 'Format 1:2',
                    'description' => '',
                ],
                'size' => [
                    'w' => 960,
                    'h' => 1920,
                ],
                'transform' => [
                    'type' => 'scalecrop',
                    'allowupscale' => false,
                ],
                'imagine' => [
                    'jpeg_quality' => 100,
                    'png_compression_level' => 7,
                ],
            ],
            '1_1' => [
                'ratio_spacer' => '1_1',
                'info' => [
                    'name' => 'Quadrat 1:1',
                    'description' => '',
                ],
                'size' => [
                    'w' => 1920,
                    'h' => 1920,
                ],
                'transform' => [
                    'type' => 'scalecrop',
                    'allowupscale' => false,
                ],
                'imagine' => [
                    'jpeg_quality' => 100,
                    'png_compression_level' => 7,
                ],
            ],
            'xl' => [
                'info' => [
                    'name' => 'Großansicht',
                    'description' => 'In der Bildergalerie',
                ],
                'size' => [
                    'w' => 1920,
                    'h' => 1920,
                ],
                'transform' => [
                    'type' => 'fitinside',
                    'allowupscale' => false,
                ],
                'imagine' => [
                    'jpeg_quality' => 100,
                    'png_compression_level' => 7,
                ],
            ],
        ],
    ],
];
