<?php

namespace App\Controller;

use App\Library\Facades\Menu;
use App\Library\Facades\Request;
use Cake\Datasource\ModelAwareTrait;
use Cake\Event\EventInterface;

class DisplayController extends SxCrudController
{
    use ModelAwareTrait;

    /**
     * @var string
     */
    protected $theme;

    /**
     * @var \App\Model\Table\MenusTable
     */
    protected $Menus;

    /**
     * Initialize controller.
     */
    public function initialize(): void
    {
        $this->loadModel('Menus');

        parent::initialize();
    }

    /**
     * @inheritdoc
     */
    function beforeRender(EventInterface $event)
    {
        $this->theme = $this->theme ?: Menu::getDomainRecord('theme');

        if ( Request::ext() === 'html' && Request::segment(0) !== 'system' ) {

            $this->set('menumain',
                $this->Menus->buildMenu('Hauptmenü'));

            $this->set('menufooter',
                $this->Menus->buildMenu('Footermenü'));

        }

        return parent::beforeRender($event);
    }

}
