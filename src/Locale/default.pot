# LANGUAGE translation of CakePHP Application
# Copyright YEAR NAME <EMAIL@ADDRESS>
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2021-04-14 10:11+0200\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: Templates/email/html/newsletter/01-default.php:12
msgid "Newsletter"
msgstr ""

#: Templates/email/html/newsletter/01-default.php:297
msgid "zur Website"
msgstr ""

#: Templates/email/html/newsletter/02-responsive.php:130
msgid "Webseite besuchen"
msgstr ""

#: Controller/Component/Authentication/SxAuthenticationComponent.php:44
#: Library/SxAuthManager.php:293
msgid "Es konnte kein Benutzer zu den angegebenen Daten gefunden werden."
msgstr ""

#: Controller/Component/Authentication/SxAuthenticationComponent.php:45
msgid "Willkommen zurück!"
msgstr ""

#: Controller/Component/Authentication/SxAuthenticationComponent.php:136
msgid "Sie haben sich erfolgreich abgemeldet."
msgstr ""

#: Controller/Component/Formular/SxBaseFormularComponent.php:153
msgid "Es sind Fehler aufgetreten, bitte überprüfen Sie Ihre Eingaben."
msgstr ""

#: Controller/Component/Formular/SxBaseFormularComponent.php:223
msgid "E-Mail wiederholen"
msgstr ""

#: Controller/Component/Formular/SxBaseFormularComponent.php:224
msgid "Notiz"
msgstr ""

#: Controller/Component/Formular/SxBaseFormularComponent.php:225
msgid "Nachname"
msgstr ""

#: Controller/Component/Formular/SxBaseFormularComponent.php:226
msgid "2. Nachricht"
msgstr ""

#: Controller/Component/Formular/SxBaseFormularComponent.php:227
msgid "2. E-Mail"
msgstr ""

#: Controller/Component/Formular/SxBaseFormularComponent.php:458
msgid "formular.formular.email.subject"
msgstr ""

#: Controller/Component/Formular/SxBaseFormularComponent.php:505
#: Controller/Component/Formular/SxBaseFormularComponent.php:510
msgid "formular."
msgstr ""

#: Controller/Component/Formular/SxCheckoutComponent.php:28
#: Controller/Component/Formular/SxCheckoutComponent.php:64
msgid "formular.checkout.cardempty.content"
msgstr ""

#: Controller/Component/Formular/SxCheckoutComponent.php:77
msgid "Mit dem gewählten Zahlungsanbieter konnte keine Zahlung durchgeführt werden. Bitte versuchen Sie es erneut oder wählen Sie eine andere Zahlungsart."
msgstr ""

#: Controller/Component/Formular/SxRegisterDefaultComponent.php:87
msgid "Diese E-Mail-Adresse ist schon registriert. Haben Sie vielleicht nur Ihr Passwort vergessen?"
msgstr ""

#: Controller/Component/SxNewsletterComponent.php:90
msgid "newsletter.bestaetigung.email.content"
msgstr ""

#: Controller/Component/SxNewsletterComponent.php:98
msgid "newsletter.bestaetigung.email.subject"
msgstr ""

#: Controller/SxAppController.php:116
#: Templates/error/error400.php:4
#: Templates/error/error403.php:4
#: Templates/error/error404.php:4
#: Templates/error/error500.php:4
msgid "Fehler"
msgstr ""

#: Controller/SxAppController.php:122
#: Controller/SxAppController.php:134
msgid "Hinweis"
msgstr ""

#: Controller/SxAppController.php:128
msgid "Warnung"
msgstr ""

#: Controller/SxCrudController.php:388
msgid "Bitte wählen Sie eine Datei aus."
msgstr ""

#: Controller/SxCrudController.php:398
msgid "Die Datei konnte nicht verschoben werden."
msgstr ""

#: Controller/SxCrudController.php:406
msgid "Irgendwas ist schief gelaufen."
msgstr ""

#: Controller/SxCrudController.php:409
msgid "Insgesamt {0} Einträgen importiert."
msgstr ""

#: Controller/SxMenusController.php:32
msgid "Alias existiert bereits auf dieser Ebene."
msgstr ""

#: Controller/Traits/InsertTrait.php:32
#: Controller/Traits/MoveTrait.php:36
msgid "Ziel konnte nicht gefunden werden."
msgstr ""

#: Controller/Traits/InsertTrait.php:51
#: Controller/Traits/MoveTrait.php:55
msgid "Einträge konnten nicht verschoben werden."
msgstr ""

#: Controller/Traits/InsertTrait.php:73
#: Controller/Traits/InsertTrait.php:121
#: Controller/Traits/MoveTrait.php:70
#: Controller/Traits/MoveTrait.php:102
msgid "Quelle konnte nicht gefunden werden."
msgstr ""

#: Form/FormdataForm.php:66
#: Form/FormdataForm.php:67
#: Form/FormdataForm.php:68
#: Form/FormdataForm.php:73
#: Form/FormdataForm.php:74
#: Form/FormdataForm.php:75
#: Form/FormdataForm.php:76
msgid "Bitte füllen Sie das Feld aus."
msgstr ""

#: Form/FormdataForm.php:69
msgid "Bitte markieren Sie die Checkbox."
msgstr ""

#: Form/FormdataForm.php:70
#: Form/FormdataForm.php:71
#: Form/FormdataForm.php:72
msgid "Bitte treffen Sie eine Auswahl."
msgstr ""

#: Form/FormdataForm.php:77
#: Form/FormdataForm.php:78
msgid "Bitte laden Sie eine Datei hoch."
msgstr ""

#: Form/FormdataForm.php:79
msgid "??"
msgstr ""

#: Library/Services/SxMailerService.php:167
msgid "email.grusszeile"
msgstr ""

#: Library/SxAuthManager.php:294
msgid "Willkommen zurück"
msgstr ""

#: Library/SxPageBuilder.php:167
msgid "search.noresults.content"
msgstr ""

#: Library/SxPageBuilder.php:168
msgid "search.noresults.headline"
msgstr ""

#: Maintenance/SxMaintenanceRenderer.php:142
#: Templates/layout/maintenance.php:10
msgid "Maintenance"
msgstr ""

#: Model/Table/SxAttachmentsTable.php:635
msgid "Processing "
msgstr ""

#: Model/Table/SxAttachmentsTable.php:655
#: Model/Table/SxAttachmentsTable.php:657
msgid "Thumbnails regenerated for "
msgstr ""

#: Model/Table/SxCrontasksTable.php:37
msgid "Bitte geben Sie einen Namen ein."
msgstr ""

#: Model/Table/SxCrontasksTable.php:42
msgid "Bitte geben Sie einen Methode ein."
msgstr ""

#: Model/Table/SxCrontasksTable.php:52
msgid "Bitte wählen Sie ein Ausführungsintervall."
msgstr ""

#: Model/Table/SxMenusTable.php:228
msgid "Bitte geben Sie eine Domain ein"
msgstr ""

#: Model/Table/SxMenusTable.php:233
#: Model/Table/SxMenusTable.php:242
#: Model/Table/SxMenusTable.php:262
msgid "Bitte geben Sie eine gültige Domain ein"
msgstr ""

#: Model/Table/SxMenusTable.php:255
msgid "Bitte geben Sie einen Namen für das Menü ein."
msgstr ""

#: Model/Table/SxNewsletterrecipientsTable.php:215
#: Templates/element/newsletter/newsletter-anrede.php:6
msgid "Sehr geehrte Damen und Herren"
msgstr ""

#: Model/Table/SxUsersTable.php:127
msgid "Bitte wählen Sie eine Stammzugehörigkeit aus."
msgstr ""

#: Model/Table/SxUsersTable.php:134
msgid "Bitte geben Sie ein Passwort ein, das den festgelegten Regeln entspricht."
msgstr ""

#: Templates/element/article/eventdate.php:18
msgid "d.m.Y"
msgstr ""

#: Templates/element/article/eventdate.php:19
msgid "H:i"
msgstr ""

#: Templates/element/article/eventdates.php:3
msgid "Alle Termine zu diesem Event"
msgstr ""

#: Templates/element/article/readmore.php:9
msgid "mehr erfahren"
msgstr ""

#: Templates/element/formular/multiselect.php:25
#: Templates/element/formular/select.php:25
msgid "Bitte auswählen"
msgstr ""

#: Templates/element/formular/requiredtext.php:4
msgid "Wir bitten Sie die mit einem * gekennzeichneten Felder auszufüllen. Vielen Dank!"
msgstr ""

#: Templates/element/formular/reworkordelete/confirm.php:23
msgid "Abbrechen"
msgstr ""

#: Templates/element/formular/reworkordelete/confirm.php:25
msgid "OK"
msgstr ""

#: Templates/element/formular/reworkordelete/confirmtext-newsletter.php:1
msgid "datenschutzerklaerung.text"
msgstr ""

#: Templates/element/formular/reworkordelete/newsletter.php:2
msgid "formfield.newsletter.text"
msgstr ""

#: Templates/element/formular/reworkordelete/payone.php:79
msgid "Es ist ein Fehler aufgetreten. Bitte versuchen Sie es noch einaml."
msgstr ""

#: Templates/element/formular/reworkordelete/voucherlayout.php:11
msgid "Auswählen"
msgstr ""

#: Templates/element/formular/reworkordelete/voucherlayout.php:11
msgid "Vorschau"
msgstr ""

#: Templates/element/formular/reworkordelete/voucherpreview.php:4
msgid "Klicken Sie hier um eine Vorschau Ihres Gutscheines zu erstellen."
msgstr ""

#: Templates/element/formular/reworkordelete/voucherpreview.php:9
msgid "Jetzt Gutschein Vorschau als PDF herunterladen."
msgstr ""

#: Templates/element/formular/reworkordelete/voucherpreview.php:12
msgid "Bitte haben Sie einen Moment Geduld. Die Gutschein Vorschau steht gleich für Sie bereit."
msgstr ""

#: Templates/element/formular/submit.php:4
msgid "Abschicken"
msgstr ""

#: Templates/element/items/event-daterange.php:30
msgid "Von"
msgstr ""

#: Templates/element/items/event-daterange.php:37
msgid "Bis"
msgstr ""

#: Templates/element/items/event-daterange.php:44
#: Templates/element/search/frontendsearch.php:12
#: Templates/element/search/search_old.php:7
msgid "Suchen"
msgstr ""

#: Templates/element/items/event.php:15
msgid "Januar"
msgstr ""

#: Templates/element/items/event.php:16
msgid "Februar"
msgstr ""

#: Templates/element/items/event.php:17
msgid "März"
msgstr ""

#: Templates/element/items/event.php:18
msgid "April"
msgstr ""

#: Templates/element/items/event.php:19
msgid "Mai"
msgstr ""

#: Templates/element/items/event.php:20
msgid "Juni"
msgstr ""

#: Templates/element/items/event.php:21
msgid "Juli"
msgstr ""

#: Templates/element/items/event.php:22
msgid "August"
msgstr ""

#: Templates/element/items/event.php:23
msgid "September"
msgstr ""

#: Templates/element/items/event.php:24
msgid "Oktober"
msgstr ""

#: Templates/element/items/event.php:25
msgid "November"
msgstr ""

#: Templates/element/items/event.php:26
msgid "Dezember"
msgstr ""

#: Templates/element/maps/directions.php:60
msgid "Anfahrt"
msgstr ""

#: Templates/element/maps/directions.php:64
msgid "Startadresse"
msgstr ""

#: Templates/element/maps/directions.php:68
msgid "Bitte geben Sie eine gültige Adresse ein"
msgstr ""

#: Templates/element/maps/directions.php:72
msgid "Berechnen"
msgstr ""

#: Templates/element/maps/list.php:85
msgid "Alle anzeigen"
msgstr ""

#: Templates/element/media/downloads.php:9
msgid "Downloads"
msgstr ""

#: Templates/element/newsletter/newsletter-anrede.php:7
msgid "Sehr geehrte/r"
msgstr ""

#: Templates/element/newsletter/newsletter-unsubscribelink.php:6
msgid "Newsletter abbestellen"
msgstr ""

#: Templates/element/newsletter/newsletter-webversionlink.php:22
msgid "Wird dieser Newsletter nicht richtig angezeigt, klicken Sie bitte hier."
msgstr ""

#: Templates/element/page-neighbors-next.php:4
msgid "Nächster Artikel"
msgstr ""

#: Templates/element/page-neighbors-prev.php:4
msgid "Vorheriger Artikel"
msgstr ""

#: Templates/element/page-neighbors-top.php:4
msgid "Zur Übersicht"
msgstr ""

#: Templates/element/paging-bar.php:26
msgid "zurück"
msgstr ""

#: Templates/element/paging-bar.php:27
msgid "Erste Seite"
msgstr ""

#: Templates/element/paging-bar.php:32
msgid "Letzte Seite"
msgstr ""

#: Templates/element/paging-bar.php:33
msgid "weiter"
msgstr ""

#: Templates/element/paging-top.php:8
msgid "Seite {{page}} von {{pages}}"
msgstr ""

#: Templates/element/search/frontendsearch.php:8
msgid "Suchbegriff"
msgstr ""

#: Templates/element/share-button.php:28
msgid "Hey,\\nwäre das nicht was für Dich?\\n\\nViele Grüße!\\n\\n"
msgstr ""

#: Templates/element/system/breadcrumb.php:12
msgid "Du bist hier"
msgstr ""

#: Templates/element/system/cookie.php:12
msgid "Diese Website nutzt Cookies, um bestmögliche Funktionalität bieten zu können."
msgstr ""

#: Templates/element/system/cookie.php:19
msgid "Notwendig"
msgstr ""

#: Templates/element/system/cookie.php:25
msgid "Performance"
msgstr ""

#: Templates/element/system/cookie.php:31
msgid "Statistik"
msgstr ""

#: Templates/element/system/cookie.php:38
msgid "Akzeptieren"
msgstr ""

#: Templates/email/html/formular.php:58
msgid "Ja"
msgstr ""

#: Templates/email/html/formular.php:60
msgid "Nein"
msgstr ""

#: Templates/error/error400.php:5
#: Templates/error/error403.php:5
msgid "Sie haben kein Zugriff auf {0}."
msgstr ""

#: Templates/error/error404.php:5
msgid "Die angeforderte Adresse {0} wurde nicht gefunden."
msgstr ""

#: Templates/error/error500.php:2
msgid "Es ist ein interner Fehler aufgetreten."
msgstr ""

#: Templates/layout/maintenance.php:18
msgid "Back"
msgstr ""

#: Templates/layout/system.php:43
msgid "Loading"
msgstr ""

#: Templates/orders/pdf/invoice.php:18
msgid "invoice.biller"
msgstr ""

#: Templates/orders/pdf/invoice.php:58
msgid "Rechnungs-Nr."
msgstr ""

#: Templates/orders/pdf/invoice.php:60
msgid "Datum"
msgstr ""

#: Templates/orders/pdf/invoice.php:62
#: Templates/vouchers/pdf/view.php:48
msgid "Seite"
msgstr ""

#: Templates/orders/pdf/invoice.php:69
msgid "Rechnung"
msgstr ""

#: Templates/orders/pdf/invoice.php:76
msgid "Menge"
msgstr ""

#: Templates/orders/pdf/invoice.php:78
msgid "Einzelpreis"
msgstr ""

#: Templates/orders/pdf/invoice.php:78
msgid "(netto)"
msgstr ""

#: Templates/orders/pdf/invoice.php:79
msgid "MwSt."
msgstr ""

#: Templates/orders/pdf/invoice.php:80
msgid "Gesamtpreis"
msgstr ""

#: Templates/orders/pdf/invoice.php:100
msgid "Versandkosten"
msgstr ""

#: Templates/orders/pdf/invoice.php:110
msgid "Zwischensumme (netto)"
msgstr ""

#: Templates/orders/pdf/invoice.php:118
msgid "Zzgl. MwSt."
msgstr ""

#: Templates/orders/pdf/invoice.php:126
msgid "Gesamtbetrag"
msgstr ""

#: Templates/orders/pdf/invoice.php:140
msgid "invoice.paid"
msgstr ""

#: Templates/orders/pdf/invoice.php:147
msgid "invoice.paymentinfo"
msgstr ""

#: Templates/orders/pdf/invoice.php:155
msgid "invoice.thanks"
msgstr ""

#: Templates/vouchers/pdf/view.php:48
msgid "Gültig bis "
msgstr ""

#: Templates/vouchers/pdf/view.php:52
msgid "Ungültig - Vorschau"
msgstr ""

#: Templates/vouchers/pdf/view.php:133
msgid "Der Gutschein enthält"
msgstr ""

#: View/Helper/SxArticleHelper.php:176
msgid "Erste"
msgstr ""

#: View/Helper/SxArticleHelper.php:180
msgid "Zurück"
msgstr ""

#: View/Helper/SxArticleHelper.php:184
msgid "Weiter"
msgstr ""

#: View/Helper/SxArticleHelper.php:188
msgid "Letzte"
msgstr ""

#: View/Helper/PaginatorHelper.php:1268
msgid "View"
msgstr ""

