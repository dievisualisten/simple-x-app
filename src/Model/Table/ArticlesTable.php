<?php

namespace App\Model\Table;

class ArticlesTable extends SxArticlesTable
{

    /**
     * @inheritdoc
     */
    public function initialize(array $config): void
    {

        $this->addElementType('faq', [
            'className' => 'Elements',
        ]);

	/*
        $this->addElementType('asp', [
            'className' => 'Elements',
        ]);
	*/

        parent::initialize($config);
    }

}
