<?php
// In src/Model/Validation/ContactValidator.php
namespace App\Model\Validation;

/*
$context Param
data: The original data passed to the validation method, useful if you plan to create rules comparing values.
providers: The complete list of rule provider objects, useful if you need to create complex rules by calling multiple providers.
newRecord: Whether the validation call is for a new record or a preexisting one.
field: fieldname
*/

use PasswordPolicy\Policy;

class PasswortValidation extends SxPass
{
    public static function checkPassword($blankNewPw)
    {

        $policy = new Policy;
        $policy->contains('lowercase', $policy->atLeast(1));
        $result = $policy->test($blankNewPw);
        $hasLowercase = $result->result;


        $rulesCheck = ((int)$hasLowercase);

        if ($rulesCheck < 1) {
            return false;
        }

        return true;
    }
}
