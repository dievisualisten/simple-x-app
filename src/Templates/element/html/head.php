<?php

$theme = menu()->getDomainRecord('theme', 'default');

$this->Asset->style([
    "dist/theme/{$theme}/vendor.css",
    "dist/theme/{$theme}/style.css",
]);

$styleAttrs = [];

echo $this->Asset->renderStyles($styleAttrs,
    "dist/cache/theme/{$theme}/style.css");
?>

<link rel="preload" href="/src/static/css/quickfix.css" as="style">
