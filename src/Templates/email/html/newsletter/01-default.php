<?
$table_width = '540';
$img_width = '160';
$content_width = $table_width - $img_width;
$elementColor = '#ff0000'; /*ZUM Layouten auf #ff0000*/
$folder = 'default'
?>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><? __('Newsletter') ?></title>
    <style type="text/css" media="screen">
        <? include (WWW_ROOT.'css'.DS.'email'.DS.'newsletter-'.$folder.'.css')?>
    </style>
</head>
<body>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center">
            <table width="520" cellspacing="0" cellpadding="0">
                <!-- Webversion link -->
                <tr>
                    <td bgcolor="#451d4f" width="520" align="center" valign="top">
                        <table width="520" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" height="30" valign="center"
                                    class="webversionlink"><?= $this->element('newsletter-webversionlink',
                                        ['options' => ['webversion' => true]]); ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- Webversion link ENDE-->

                <?
                //Ab hier wird der Content gesammelt und die Links werden umgeschrieben
                ob_start();
                ?>

                <!-- Logo -->
                <tr>
                    <td align="center"><a href="<?= $base_href ?>"><img class="logo-img"
                                                                        src="<?= $base_href ?>/img/email/newsletter/<?= $folder ?>/logo.gif"
                                                                        width="660" height="100" alt=""/></a></td>
                </tr>
                <!-- Logo ENDE -->

                <!-- Start Content Bereich !-->

                <tr>
                    <td>

                        <table width="660" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="hidden-xs" width="40"><img class="spacer"
                                                                      src="<?= $base_href ?>/img/email/newsletter/<?= $folder ?>/spacer.gif"
                                                                      width="40" height="40" alt=""/></td>
                                <td>

                                    <!-- titleimg START -->
                                    <table width="580" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="title-img" align="center"><!-- Bild aus dem Hauptartikel -->
                                                <?= $this->element('img', [
                                                    'options' => [
                                                        'link' => true,
                                                        'linkoptions' => ['url' => $base_href],
                                                        'wrap' => false,
                                                        'show' => 'titleimg',
                                                        'folder' => 'newsletter-title',
                                                    ],
                                                ]);
                                                ?>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- titleimg ENDE -->

                                    <!-- Content -->
                                    <table class="newsletter-content" width="580" border="0" cellspacing="0"
                                           cellpadding="0">
                                        <tr>
                                            <td width="40"><img class="spacer"
                                                                src="<?= $base_href ?>/img/email/newsletter/<?= $folder ?>/spacer.gif"
                                                                width="40" height="30" alt=""/></td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top" class="content">
                                                <?= $this->element('h2'); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top"><img
                                                    src="<?= $base_href ?>/img/email/newsletter/<?= $folder ?>/linie.gif"
                                                    width="40" height="29" alt=""/></td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top" class="content">
                                                <?= $this->element('article/content'); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="40"><img class="spacer"
                                                                src="<?= $base_href ?>/img/email/newsletter/<?= $folder ?>/spacer.gif"
                                                                width="40" height="25" alt=""/></td>
                                        </tr>
                                    </table>
                                    <!-- Content Ende -->


                                    <!-- Listitems -->

                                    <? if (!empty($children)) { ?>

                                        <?php foreach ($children as $item) { ?>

                                            <!-- Teasercontainer -->

                                            <!-- Teaserrow -->
                                            <table width="<?= $content_width ?>" border="0" cellspacing="0"
                                                   cellpadding="0" style="border: 1px solid #6a3478;">
                                                <!-- IMG -->
                                                <tr>
                                                    <td>
                                                        <table width="<?= $content_width ?>" border="0" cellspacing="0"
                                                               cellpadding="0">
                                                            <tr>
                                                                <td width="18"><img class="spacer"
                                                                                    src="<?= $base_href ?>/img/email/newsletter/<?= $folder ?>/spacer.gif"
                                                                                    width="18" height="18" alt=""/></td>
                                                                <td><img class="spacer"
                                                                         src="<?= $base_href ?>/img/email/newsletter/<?= $folder ?>/spacer.gif"
                                                                         width="18" height="18" alt=""/></td>
                                                                <td width="18"><img class="spacer"
                                                                                    src="<?= $base_href ?>/img/email/newsletter/<?= $folder ?>/spacer.gif"
                                                                                    width="18" height="18" alt=""/></td>
                                                            </tr>
                                                            <tr>
                                                                <td width="19"><img class="spacer"
                                                                                    src="<?= $base_href ?>/img/email/newsletter/<?= $folder ?>/spacer.gif"
                                                                                    width="19" height="19" alt=""/></td>
                                                                <td valign="top" class="teaserimg" width="520">
                                                                    <?= $this->element('img', [
                                                                        'options' => [
                                                                            'data' => $item,
                                                                            'link' => true,
                                                                            'wrap' => false,
                                                                            'show' => ['teaser', 'titleimg'],
                                                                            'folder' => 'newsletter-teaser',
                                                                        ],
                                                                    ]);
                                                                    ?>
                                                                </td>
                                                                <td width="19"><img class="spacer"
                                                                                    src="<?= $base_href ?>/img/email/newsletter/<?= $folder ?>/spacer.gif"
                                                                                    width="19" height="19" alt=""/></td>
                                                            </tr>
                                                            <tr>
                                                                <td width="18"><img class="spacer"
                                                                                    src="<?= $base_href ?>/img/email/newsletter/<?= $folder ?>/spacer.gif"
                                                                                    width="18" height="18" alt=""/></td>
                                                                <td><img class="spacer"
                                                                         src="<?= $base_href ?>/img/email/newsletter/<?= $folder ?>/spacer.gif"
                                                                         width="18" height="18" alt=""/></td>
                                                                <td width="18"><img class="spacer"
                                                                                    src="<?= $base_href ?>/img/email/newsletter/<?= $folder ?>/spacer.gif"
                                                                                    width="18" height="18" alt=""/></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <!-- IMG END -->

                                                <!-- Teasecontent -->
                                                <tr>
                                                    <td class="teasercontent" valign="top"
                                                        width="<?= $content_width ?>">

                                                        <table width="<?= $content_width ?>" border="0" cellspacing="0"
                                                               cellpadding="0">

                                                            <!-- Teaser Text  -->
                                                            <tr>
                                                                <td width="40"><img class="spacer"
                                                                                    src="<?= $base_href ?>/img/email/newsletter/<?= $folder ?>/spacer.gif"
                                                                                    width="40" height="7" alt=""/></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table width="<?= $content_width ?>" border="0"
                                                                           cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td align="center" valign="top"
                                                                                class="teasercontent"
                                                                                style="padding: 0 19px;">
                                                                                <h3><?= $this->Fe->link($item); ?></h3>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><img class="spacer"
                                                                                     src="<?= $base_href ?>/img/email/newsletter/<?= $folder ?>/spacer.gif"
                                                                                     width="40" height="14" alt=""/>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center" valign="top"
                                                                                style="padding: 0 19px;"><?= $this->element('article/teasertext',
                                                                                    [
                                                                                        'options' => [
                                                                                            'showMore' => false,
                                                                                            'baseHref' => $base_href,
                                                                                        ],
                                                                                        'data' => $item,
                                                                                    ]); ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><img class="spacer"
                                                                                     src="<?= $base_href ?>/img/email/newsletter/<?= $folder ?>/spacer.gif"
                                                                                     width="40" height="14" alt=""/>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center" valign="top"><span
                                                                                    class="more"><?= $this->Fe->link($item,
                                                                                        [
                                                                                            'linktext' => '[ mehr erfahren ]',
                                                                                            'class' => 'more',
                                                                                        ]) ?></span></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><img class="spacer"
                                                                                     src="<?= $base_href ?>/img/email/newsletter/<?= $folder ?>/spacer.gif"
                                                                                     width="40" height="30" alt=""/>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <!-- Teaser Text  ENDE -->

                                                        </table>
                                                    </td>
                                                    <!-- Teasecontent ENDE-->
                                                </tr>
                                            </table>

                                            <table width="<?= $content_width ?>" border="0" cellspacing="0"
                                                   cellpadding="0">
                                                <tr>
                                                    <td width="40"><img class="spacer"
                                                                        src="<?= $base_href ?>/img/email/newsletter/<?= $folder ?>/spacer.gif"
                                                                        width="40" height="20" alt=""/></td>
                                                </tr>
                                            </table>
                                            <!-- Teaserrow ENDE -->

                                            <!-- Teasercontainer ENDE -->

                                        <? } ?>

                                    <? } ?>

                                    <!-- Listitems ENDE -->

                                    <!-- Footer Start -->

                                    <table width="<?= $content_width ?>" border="0" cellspacing="0" cellpadding="0"
                                           style="background: #562463;">
                                        <?
                                        //Ausgabe vor dem Abmeldelink und vor dem Trackingpixel;
                                        $ausgabe = ob_get_contents();
                                        ob_end_clean();
                                        echo $this->Fe->rewriteUrl($ausgabe, $newsletterrecipient, $base_href);
                                        ?>

                                        <?= $this->element('newsletter-footer'); ?>

                                        <!-- Spacer -->
                                        <tr>
                                            <td colspan="2" align="center" valign="top">
                                                <table width="520" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="520"><img
                                                                src="<?= $base_href ?>/img/email/newsletter/<?= $folder ?>/spacer.gif"
                                                                width="1" height="16" alt=""/></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <!-- Spacer ENDE -->
                                        <tr>
                                            <td align="left" valign="center" class="linkslink"
                                                style="padding-left: 20px;"><a
                                                    href="<?= $base_href ?>"><? echo __("zur Website"); ?></a></td>
                                            <td align="right" valign="center" class="linkslink"
                                                style="padding-right: 20px;"><?= $this->element('newsletter-abmeldelink'); ?></td>
                                        </tr>
                                        <!-- Spacer -->
                                        <tr>
                                            <td colspan="2" align="center" valign="top"
                                                style="border-bottom: 1px solid #000;">
                                                <table width="520" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="520"><img
                                                                src="<?= $base_href ?>/img/email/newsletter/<?= $folder ?>/spacer.gif"
                                                                width="1" height="14" alt=""/></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="center" valign="top">
                                                <table width="520" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="520"><img
                                                                src="<?= $base_href ?>/img/email/newsletter/<?= $folder ?>/spacer.gif"
                                                                width="1" height="16" alt=""/></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <!-- Spacer ENDE -->
                                        <tr>
                                            <td colspan="2" align="center" class="footer">

                                                <b>&copy; Kunde </b><br>
                                                Strasse | Ort | Tel<br>
                                                <a href="http://www.website.de">Website</a>
                                            </td>
                                        </tr>
                                        <!-- Spacer -->
                                        <tr>
                                            <td colspan="2" align="center" valign="top">
                                                <table width="520" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="520"><img
                                                                src="<?= $base_href ?>/img/email/newsletter/<?= $folder ?>/spacer.gif"
                                                                width="1" height="14" alt=""/></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <!-- Spacer ENDE -->
                                        <tr>
                                            <td colspan="2" align="center" class="teaserimg">
                                                <img
                                                    src="<?= $base_href ?>/img/email/newsletter/<?= $folder ?>/footer.gif"
                                                    width="580" height="85" alt=""/>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- Footer ENDE -->
                                </td>
                                <td class="hidden-xs" width="40"><img class="spacer"
                                                                      src="<?= $base_href ?>/img/email/newsletter/<?= $folder ?>/spacer.gif"
                                                                      width="40" height="40" alt=""/></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- Start Content Bereich !-->
                <tr>
                    <td align="center" class="copyright">
                        Powered by simple X CMS © Die Visualisten GmbH
                    </td>
                </tr>
                <!-- Spacer -->
                <tr>
                    <td align="center" valign="top">
                        <table width="520" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="520"><img class="spacer"
                                                     src="<?= $base_href ?>/img/email/newsletter/<?= $folder ?>/spacer.gif"
                                                     width="1" height="20" alt=""/></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- Spacer ENDE -->
            </table>
        </td>
    </tr>
</table>
<?= $this->element('newsletter-opener'); ?>
</body>
</html>
