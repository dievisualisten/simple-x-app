<?
$options = [
    'table_width' => 520,
    'img_width' => 180,
    'content_width' => 520,
    'list_width' => 320,
    'folder' => 'responsive',
    'base_href' => $base_href,
];

extract($options);

?>
<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <title><? $newsletter->subject ?></title>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Coiny');
    </style>

    <!--[if mso]>

    <style>

    </style>

    <![endif]-->
    <style type="text/css" media="screen">
        <? include (WWW_ROOT.'css'.DS.'email'.DS.'newsletter-'.$folder.'.css')?>
    </style>
</head>
<body>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center" valign="top">
            <table width="<?= $table_width ?>" cellspacing="0" cellpadding="0">
                <!-- Webversion link -->
                <tr>
                    <td align="center" style="padding-top: 30px; padding-bottom: 10px;" valign="center"
                        class="webversionlink"><?= $this->element('newsletter-webversionlink',
                            ['options' => ['webversion' => true]]); ?></td>
                </tr>
                <!-- Webversion link ENDE-->

                <?
                //Ab hier wird der Content gesammelt und die Links werden umgeschrieben zum zählen
                ob_start();
                ?>

                <?= $this->element('newsletter-intro', ['options' => $options]); ?>
                <?= $this->element('newsletter-trenner',
                    ['options' => ['folder' => $folder, 'content_width' => $content_width]]); ?>

                <? if (!empty($children)) {
                    foreach ($children as $child) {

                        // Einzelnes Listenelement
                        if ($child->teaser == 2) {
                            echo $this->element('newsletter-item-list', [
                                'options' => Hash::merge($options, [
                                    'data' => $child,
                                ]),
                            ]);
                        }

                        // Einzelner Teaser
                        if ($child->teaser == 4) {
                            echo $this->element('newsletter-item-teaser', [
                                'options' => Hash::merge($options, [
                                    'data' => $child,
                                ]),
                            ]);
                        }

                        // Abschnitt
                        if ($child->teaser == 1024) {
                            echo $this->element('newsletter-abschnitt', [
                                'options' => Hash::merge($options, [
                                    'data' => $child,
                                ]),
                            ]);

                            //Kinder des Abschnitts
                            if (!empty($child->children)) {
                                foreach ($child->children as $childChild) {

                                    if ($childChild->teaser == 32) {
                                        echo $this->element('newsletter-item-list', [
                                            'options' => Hash::merge($options, [
                                                'data' => $childChild,
                                            ]),
                                        ]);
                                    }

                                    if ($childChild->teaser == 4) {
                                        echo $this->element('newsletter-item-teaser', [
                                            'options' => Hash::merge($options, [
                                                'data' => $childChild,
                                            ]),
                                        ]);
                                    }
                                }
                            }
                        }
                    }
                } ?>

                <!-- Footer -->
                <tr>
                    <td class="content">
                        <table width="<?= $content_width ?>" border="0" cellspacing="0" cellpadding="0">
                            <?
                            //Ausgabe vor dem Abmeldelink und vor dem Trackingpixel;
                            $ausgabe = ob_get_contents();
                            ob_end_clean();
                            echo $this->Fe->rewriteUrl($ausgabe, $newslettercampaign, $base_href);
                            ?>

                            <tr>
                                <td colspan="2" align="center" valign="top">
                                    <?= $this->element('newsletter-spacer', ['options' => ['height' => 16]]); ?>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="center" class="linkslink left"><a
                                        href="<?= $base_href ?>"><?= __("Webseite besuchen"); ?></a></td>
                                <td align="right" valign="center"
                                    class="linkslink right"><?= $this->element('newsletter-unsubscribelink'); ?></td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center" valign="top"> <?= $this->element('newsletter-spacer',
                                        ['options' => ['height' => 30]]); ?></td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center" class="footer">
                                    <b>&copy; <?= date('Y') ?> Die Visualisten GmbH</b><br>
                                    Hirschgraben 30 | 22089 Hamburg<br>
                                    <a href="mailto:info@dievisualisten.com">info@dievisualisten.com</a> | <a
                                        href="http://www.dievisualisten.com" target="_blank">www.dievisualisten.com</a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center" valign="top">
                                    <?= $this->element('newsletter-spacer', ['options' => ['height' => 14]]); ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- LINKS ENDE -->
                <tr>
                    <td align="center" valign="top">
                        <?= $this->element('newsletter-spacer', ['options' => ['height' => 20]]); ?>
                    </td>
                </tr>
                <tr>
                    <td align="center" class="copyright">
                        Powered by simple X CMS © <a href="http://www.dievisualisten.com" target="_blank">Die
                            Visualisten GmbH</a>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <?= $this->element('newsletter-spacer', ['options' => ['height' => 20]]); ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<?= $this->element('newsletter-opener'); ?>
</body>
</html>
