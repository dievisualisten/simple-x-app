<!DOCTYPE html>
<html lang="<?= language()->getLanguage() ?>">
<head>
    <?=$this->Html->charset(); ?>
    <title><?= __('Maintenance') ?> - <?php echo config('Sx.app.cmsname'); ?></title>
    <?= $this->element('html/head'); ?>
</head>
<body class="layout-maintenance">
    <div class="maintenance">
        <div class="maintenance__icon">
            <!-- Icon -->
        </div>
        <div class="maintenance__header">
            <h1><?= __('Maintenance') ?></h1>
        </div>
        <div class="maintenance__content">
            <?= $this->Flash->render() ?>

            <?= $this->fetch('content') ?>
        </div>
    </div>
</body>
</html>
