#AuthType Basic
#AuthName "Noch ist hier nichts zu sehen"
#AuthUserFile /kunden/creatic-design.de/rp-hosting/KUNDENNR/AUFTRAGNR/webseiten/www.client.de/app/webroot/.htpasswd
#require valid-user

# ######################################################################
# # DV ENVIORNMENTS VARS                                               #
# ######################################################################


<IfModule mod_rewrite.c>
  RewriteEngine on

  # SET UP ENVIORNMENTS

  # Default environment is production
  RewriteRule .* - [E=ENVIRONMENT:production]
  RewriteRule .* - [E=USEWWW:on]

  # Environment is set to develop if the server names ends in local.com
  RewriteCond %{SERVER_NAME} .local$
  RewriteRule .* - [E=ENVIRONMENT:local]

  RewriteCond %{SERVER_NAME} .local$
  RewriteRule .* - [E=USEWWW:off]

  # END SET UP ENVIRONMENTS
</IfModule>



# Apache Server Configs v4.0.0 | MIT License
# https://github.com/h5bp/server-configs-apache

# (!) Using `.htaccess` files slows down Apache, therefore, if you have
# access to the main server configuration file (which is usually called
# `httpd.conf`), you should add this logic there.
#
# https://httpd.apache.org/docs/current/howto/htaccess.html

# ######################################################################
# # CROSS-ORIGIN                                                       #
# ######################################################################

# ----------------------------------------------------------------------
# | Cross-origin requests                                              |
# ----------------------------------------------------------------------

# Allow cross-origin requests.
#
# https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS
# https://enable-cors.org/
# https://www.w3.org/TR/cors/

# (!) Do not use this without understanding the consequences.
#     This will permit access from any other website.
#     Instead of using this file, consider using a specific rule such as
#     allowing access based on (sub)domain:
#
#         Header set Access-Control-Allow-Origin "subdomain.example.com"

# <IfModule mod_headers.c>
#     Header set Access-Control-Allow-Origin "*"
# </IfModule>

# ----------------------------------------------------------------------
# | Cross-origin images                                                |
# ----------------------------------------------------------------------

# Send the CORS header for images when browsers request it.
#
# https://developer.mozilla.org/en-US/docs/Web/HTML/CORS_enabled_image
# https://blog.chromium.org/2011/07/using-cross-domain-images-in-webgl-and.html

<IfModule mod_setenvif.c>
    <IfModule mod_headers.c>
        <FilesMatch "\.(bmp|cur|gif|ico|jpe?g|a?png|svgz?|webp)$">
            SetEnvIf Origin ":" IS_CORS
            Header set Access-Control-Allow-Origin "*" env=IS_CORS
        </FilesMatch>
    </IfModule>
</IfModule>

# ----------------------------------------------------------------------
# | Cross-origin web fonts                                             |
# ----------------------------------------------------------------------

# Allow cross-origin access to web fonts.
#
# https://developers.google.com/fonts/docs/troubleshooting

<IfModule mod_headers.c>
    <FilesMatch "\.(eot|otf|tt[cf]|woff2?)$">
        Header set Access-Control-Allow-Origin "*"
    </FilesMatch>
</IfModule>

# ----------------------------------------------------------------------
# | Cross-origin resource timing                                       |
# ----------------------------------------------------------------------

# Allow cross-origin access to the timing information for all resources.
#
# If a resource isn't served with a `Timing-Allow-Origin` header that would
# allow its timing information to be shared with the document, some of the
# attributes of the `PerformanceResourceTiming` object will be set to zero.
#
# https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Timing-Allow-Origin
# https://www.w3.org/TR/resource-timing/
# https://www.stevesouders.com/blog/2014/08/21/resource-timing-practical-tips/

# <IfModule mod_headers.c>
#     Header set Timing-Allow-Origin: "*"
# </IfModule>

# ######################################################################
# # INTERNET EXPLORER                                                  #
# ######################################################################

# ----------------------------------------------------------------------
# | Document modes                                                     |
# ----------------------------------------------------------------------

# Force Internet Explorer 8/9/10 to render pages in the highest mode
# available in various cases when it may not.
#
# https://hsivonen.fi/doctype/#ie8
#
# (!) Starting with Internet Explorer 11, document modes are deprecated.
#     If your business still relies on older web apps and services that were
#     designed for older versions of Internet Explorer, you might want to
#     consider enabling `Enterprise Mode` throughout your company.
#
# https://msdn.microsoft.com/en-us/library/ie/bg182625.aspx#docmode
# https://blogs.msdn.microsoft.com/ie/2014/04/02/stay-up-to-date-with-enterprise-mode-for-internet-explorer-11/
# https://msdn.microsoft.com/en-us/library/ff955275.aspx

<IfModule mod_headers.c>
    #Header always set X-UA-Compatible "IE=edge" "expr=%{CONTENT_TYPE} =~ m#text/html#i"
</IfModule>

# ######################################################################
# # MEDIA TYPES AND CHARACTER ENCODINGS                                #
# ######################################################################

# ----------------------------------------------------------------------
# | Media types                                                        |
# ----------------------------------------------------------------------

# Serve resources with the proper media types (f.k.a. MIME types).
#
# https://www.iana.org/assignments/media-types/media-types.xhtml
# https://httpd.apache.org/docs/current/mod/mod_mime.html#addtype

<IfModule mod_mime.c>

  # Data interchange

    AddType application/atom+xml                        atom
    AddType application/json                            json map topojson
    AddType application/ld+json                         jsonld
    AddType application/rss+xml                         rss
    AddType application/geo+json                        geojson
    AddType application/rdf+xml                         rdf
    AddType application/xml                             xml


  # JavaScript

    # Servers should use text/javascript for JavaScript resources.
    # https://html.spec.whatwg.org/multipage/scripting.html#scriptingLanguages

    AddType text/javascript                             js mjs


  # Manifest files

    AddType application/manifest+json                   webmanifest
    AddType application/x-web-app-manifest+json         webapp
    AddType text/cache-manifest                         appcache


  # Media files

    AddType audio/mp4                                   f4a f4b m4a
    AddType audio/ogg                                   oga ogg opus
    AddType image/bmp                                   bmp
    AddType image/svg+xml                               svg svgz
    AddType image/webp                                  webp
    AddType video/mp4                                   f4v f4p m4v mp4
    AddType video/ogg                                   ogv
    AddType video/webm                                  webm
    AddType video/x-flv                                 flv

    # Serving `.ico` image files with a different media type prevents
    # Internet Explorer from displaying them as images:
    # https://github.com/h5bp/html5-boilerplate/commit/37b5fec090d00f38de64b591bcddcb205aadf8ee

    AddType image/x-icon                                cur ico


  # WebAssembly

    AddType application/wasm                            wasm


  # Web fonts

    AddType font/woff                                   woff
    AddType font/woff2                                  woff2
    AddType application/vnd.ms-fontobject               eot
    AddType font/ttf                                    ttf
    AddType font/collection                             ttc
    AddType font/otf                                    otf


  # Other

    AddType application/octet-stream                    safariextz
    AddType application/x-bb-appworld                   bbaw
    AddType application/x-chrome-extension              crx
    AddType application/x-opera-extension               oex
    AddType application/x-xpinstall                     xpi
    AddType text/calendar                               ics
    AddType text/markdown                               markdown md
    AddType text/vcard                                  vcard vcf
    AddType text/vnd.rim.location.xloc                  xloc
    AddType text/vtt                                    vtt
    AddType text/x-component                            htc

</IfModule>

# ----------------------------------------------------------------------
# | Character encodings                                                |
# ----------------------------------------------------------------------

# Serve all resources labeled as `text/html` or `text/plain` with the media type
# `charset` parameter set to `UTF-8`.
#
# https://httpd.apache.org/docs/current/mod/core.html#adddefaultcharset

AddDefaultCharset utf-8

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# Serve the following file types with the media type `charset` parameter set to
# `UTF-8`.
#
# https://httpd.apache.org/docs/current/mod/mod_mime.html#addcharset

<IfModule mod_mime.c>
    AddCharset utf-8 .appcache \
                     .bbaw \
                     .css \
                     .htc \
                     .ics \
                     .js \
                     .json \
                     .manifest \
                     .map \
                     .markdown \
                     .md \
                     .mjs \
                     .topojson \
                     .vtt \
                     .vcard \
                     .vcf \
                     .webmanifest \
                     .xloc
</IfModule>

# ----------------------------------------------------------------------
# | File access                                                        |
# ----------------------------------------------------------------------

# Block access to directories without a default document.
#
# You should leave the following uncommented, as you shouldn't allow anyone to
# surf through every directory on your server (which may include rather
# private places such as the CMS's directories).

<IfModule mod_autoindex.c>
    Options -Indexes
</IfModule>

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# Block access to all hidden files and directories except for the
# visible content from within the `/.well-known/` hidden directory.
#
# These types of files usually contain user preferences or the preserved state
# of a utility, and can include rather private places like, for example, the
# `.git` or `.svn` directories.
#
# The `/.well-known/` directory represents the standard (RFC 5785) path prefix
# for "well-known locations" (e.g.: `/.well-known/manifest.json`,
# `/.well-known/keybase.txt`), and therefore, access to its visible content
# should not be blocked.
#
# https://www.mnot.net/blog/2010/04/07/well-known
# https://tools.ietf.org/html/rfc5785

<IfModule mod_rewrite.c>
    RewriteEngine On
    RewriteCond %{REQUEST_URI} "!(^|/)\.well-known/([^./]+./?)+$" [NC]
    RewriteCond %{SCRIPT_FILENAME} -d [OR]
    RewriteCond %{SCRIPT_FILENAME} -f
    RewriteRule "(^|/)\." - [F]
</IfModule>

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# Block access to files that can expose sensitive information.
#
# By default, block access to backup and source files that may be left by some
# text editors and can pose a security risk when anyone has access to them.
#
# https://feross.org/cmsploit/
#
# (!) Update the `<FilesMatch>` regular expression from below to include any
#     files that might end up on your production server and can expose
#     sensitive information about your website. These files may include:
#     configuration files, files that contain metadata about the project (e.g.:
#     project dependencies, build scripts, etc.).

<IfModule mod_authz_core.c>
    <FilesMatch "(^#.*#|\.(bak|conf|dist|fla|in[ci]|log|orig|psd|sh|sql|sw[op])|~)$">
        Require all denied
    </FilesMatch>
</IfModule>

# ----------------------------------------------------------------------
# | Content Type Options                                               |
# ----------------------------------------------------------------------

# Prevent some browsers from MIME-sniffing the response.
#
# This reduces exposure to drive-by download attacks and cross-origin data
# leaks, and should be left uncommented, especially if the server is serving
# user-uploaded content or content that could potentially be treated as
# executable by the browser.
#
# https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Content-Type-Options
# https://blogs.msdn.microsoft.com/ie/2008/07/02/ie8-security-part-v-comprehensive-protection/
# https://mimesniff.spec.whatwg.org/

<IfModule mod_headers.c>
    Header always set X-Content-Type-Options "nosniff"
</IfModule>

# ----------------------------------------------------------------------
# | Cross-Site Scripting (XSS) Protection                              |
# ----------------------------------------------------------------------

# Protect website reflected Cross-Site Scripting (XSS) attacks.
#
# (1) Try to re-enable the cross-site scripting (XSS) filter built into most
#     web browsers.
#
#     The filter is usually enabled by default, but in some cases, it may be
#     disabled by the user. However, in Internet Explorer, for example, it can be
#     re-enabled just by sending the  `X-XSS-Protection` header with the value
#     of `1`.
#
# (2) Prevent web browsers from rendering the web page if a potential reflected
#     (a.k.a non-persistent) XSS attack is detected by the filter.
#
#     By default, if the filter is enabled and browsers detect a reflected XSS
#     attack, they will attempt to block the attack by making the smallest
#     possible modifications to the returned web page.
#
#     Unfortunately, in some browsers (e.g.: Internet Explorer), this default
#     behavior may allow the XSS filter to be exploited. Therefore, it's better
#     to inform browsers to prevent the rendering of the page altogether,
#     instead of attempting to modify it.
#
#     https://hackademix.net/2009/11/21/ies-xss-filter-creates-xss-vulnerabilities
#
# (!) Do not rely on the XSS filter to prevent XSS attacks! Ensure that you are
#     taking all possible measures to prevent XSS attacks, the most obvious
#     being: validating and sanitizing your website's inputs.
#
# https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-XSS-Protection
# https://blogs.msdn.microsoft.com/ie/2008/07/02/ie8-security-part-iv-the-xss-filter/
# https://blogs.msdn.microsoft.com/ieinternals/2011/01/31/controlling-the-xss-filter/
# https://www.owasp.org/index.php/Cross-site_Scripting_%28XSS%29

# <IfModule mod_headers.c>
#     #                           (1)    (2)
#     Header always set X-XSS-Protection "1; mode=block" "expr=%{CONTENT_TYPE} =~ m#text/html#i"
# </IfModule>

# ----------------------------------------------------------------------
# | Referrer Policy                                                    |
# ----------------------------------------------------------------------

# Set a strict Referrer Policy to mitigate information leakage.
#
# (1) The `Referrer-Policy` header is included in responses for resources
#     that are able to request (or navigate to) other resources.
#
#     This includes the commonly used resource types:
#     HTML, CSS, XML/SVG, PDF documents, scripts and workers.
#
# To prevent referrer leakage entirely, specify the `no-referrer` value
# instead. Note that the effect could impact analytics metrics negatively.
#
# To check your Referrer Policy, you can use an online service, such as:
# https://securityheaders.com/
# https://observatory.mozilla.org/
#
# https://scotthelme.co.uk/a-new-security-header-referrer-policy/
# https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referrer-Policy

# <IfModule mod_headers.c>
#     #                                                             (1)
#     Header always set Referrer-Policy "strict-origin-when-cross-origin" "expr=%{CONTENT_TYPE} =~ m#text\/(css|html|javascript)|application\/pdf|xml#i"
# </IfModule>

# ----------------------------------------------------------------------
# | Disable TRACE HTTP Method                                          |
# ----------------------------------------------------------------------

# Prevent Apache from responding to `TRACE` HTTP request.
#
# The TRACE method, while seemingly harmless, can be successfully leveraged
# in some scenarios to steal legitimate users' credentials.
#
# Modern browsers now prevent TRACE requests being made via JavaScript,
# however, other ways of sending TRACE requests with browsers have been
# discovered, such as using Java.
#
# (!) If you have access to the main server configuration file, use the
#     `TraceEnable` directive instead.
#
# https://tools.ietf.org/html/rfc7231#section-4.3.8
# https://www.owasp.org/index.php/Cross_Site_Tracing
# https://www.owasp.org/index.php/Test_HTTP_Methods_(OTG-CONFIG-006)
# https://httpd.apache.org/docs/current/mod/core.html#traceenable

# <IfModule mod_rewrite.c>
#     RewriteEngine On
#     RewriteCond %{REQUEST_METHOD} ^TRACE [NC]
#     RewriteRule .* - [R=405,L]
# </IfModule>

# ----------------------------------------------------------------------
# | Server-side technology information                                 |
# ----------------------------------------------------------------------

# Remove the `X-Powered-By` response header that:
#
#  * is set by some frameworks and server-side languages (e.g.: ASP.NET, PHP),
#    and its value contains information about them (e.g.: their name, version
#    number)
#
#  * doesn't provide any value to users, contributes to header bloat, and in
#    some cases, the information it provides can expose vulnerabilities
#
# (!) If you can, you should disable the `X-Powered-By` header from the
#     language/framework level (e.g.: for PHP, you can do that by setting
#     `expose_php = off` in `php.ini`).
#
# https://php.net/manual/en/ini.core.php#ini.expose-php

<IfModule mod_headers.c>
    Header unset X-Powered-By
    Header always unset X-Powered-By
</IfModule>

# ----------------------------------------------------------------------
# | Server software information                                        |
# ----------------------------------------------------------------------

# Prevent Apache from adding a trailing footer line containing information
# about the server to the server-generated documents (e.g.: error messages,
# directory listings, etc.).
#
# https://httpd.apache.org/docs/current/mod/core.html#serversignature

ServerSignature Off

# ######################################################################
# # WEB PERFORMANCE                                                    #
# ######################################################################

# ----------------------------------------------------------------------
# | Compression                                                        |
# ----------------------------------------------------------------------

<IfModule mod_deflate.c>

    # Force compression for mangled `Accept-Encoding` request headers
    #
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Accept-Encoding
    # https://calendar.perfplanet.com/2010/pushing-beyond-gzipping/

    <IfModule mod_setenvif.c>
        <IfModule mod_headers.c>
            SetEnvIfNoCase ^(Accept-EncodXng|X-cept-Encoding|X{15}|~{15}|-{15})$ ^((gzip|deflate)\s*,?\s*)+|[X~-]{4,13}$ HAVE_Accept-Encoding
            RequestHeader append Accept-Encoding "gzip,deflate" env=HAVE_Accept-Encoding
        </IfModule>
    </IfModule>

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    # Compress all output labeled with one of the following media types.
    #
    # https://httpd.apache.org/docs/current/mod/mod_filter.html#addoutputfilterbytype

    <IfModule mod_filter.c>
        AddOutputFilterByType DEFLATE "application/atom+xml" \
                                      "application/javascript" \
                                      "application/json" \
                                      "application/ld+json" \
                                      "application/manifest+json" \
                                      "application/rdf+xml" \
                                      "application/rss+xml" \
                                      "application/schema+json" \
                                      "application/geo+json" \
                                      "application/vnd.ms-fontobject" \
                                      "application/wasm" \
                                      "application/x-font-ttf" \
                                      "application/x-javascript" \
                                      "application/x-web-app-manifest+json" \
                                      "application/xhtml+xml" \
                                      "application/xml" \
                                      "font/eot" \
                                      "font/opentype" \
                                      "font/otf" \
                                      "font/ttf" \
                                      "image/bmp" \
                                      "image/svg+xml" \
                                      "image/vnd.microsoft.icon" \
                                      "text/cache-manifest" \
                                      "text/calendar" \
                                      "text/css" \
                                      "text/html" \
                                      "text/javascript" \
                                      "text/plain" \
                                      "text/markdown" \
                                      "text/vcard" \
                                      "text/vnd.rim.location.xloc" \
                                      "text/vtt" \
                                      "text/x-component" \
                                      "text/x-cross-domain-policy" \
                                      "text/xml"

    </IfModule>

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    # Map the following filename extensions to the specified encoding type in
    # order to make Apache serve the file types with the appropriate
    # `Content-Encoding` response header (do note that this will NOT make
    # Apache compress them!).
    #
    # If these files types would be served without an appropriate
    # `Content-Encoding` response header, client applications (e.g.: browsers)
    # wouldn't know that they first need to uncompress the response, and thus,
    # wouldn't be able to understand the content.
    #
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Encoding
    # https://httpd.apache.org/docs/current/mod/mod_mime.html#addencoding

    <IfModule mod_mime.c>
        AddEncoding gzip              svgz
    </IfModule>

</IfModule>


# ----------------------------------------------------------------------
# | ETags                                                              |
# ----------------------------------------------------------------------

# Remove `ETags` as resources are sent with far-future expires headers.
#
# https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/ETag
# https://developer.yahoo.com/performance/rules.html#etags
# https://tools.ietf.org/html/rfc7232#section-2.3

# `FileETag None` doesn't work in all cases.
<IfModule mod_headers.c>
    Header unset ETag
</IfModule>

FileETag None

# ----------------------------------------------------------------------
# | Cache expiration                                                   |
# ----------------------------------------------------------------------

# Serve resources with a far-future expiration date.
#
# (!) If you don't control versioning with filename-based cache busting, you
# should consider lowering the cache times to something like one week.
#
# https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control
# https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Expires
# https://httpd.apache.org/docs/current/mod/mod_expires.html

<IfModule mod_expires.c>

    ExpiresActive on
    ExpiresDefault                                      "access plus 1 year"

  # CSS

    ExpiresByType text/css                              "access plus 1 year"


  # Data interchange

    ExpiresByType application/atom+xml                  "access plus 1 hour"
    ExpiresByType application/rdf+xml                   "access plus 1 hour"
    ExpiresByType application/rss+xml                   "access plus 1 hour"

    ExpiresByType application/json                      "access plus 0 seconds"
    ExpiresByType application/ld+json                   "access plus 0 seconds"
    ExpiresByType application/schema+json               "access plus 0 seconds"
    ExpiresByType application/geo+json                  "access plus 0 seconds"
    ExpiresByType application/xml                       "access plus 0 seconds"
    ExpiresByType text/calendar                         "access plus 0 seconds"
    ExpiresByType text/xml                              "access plus 0 seconds"


  # Favicon (cannot be renamed!) and cursor images

    ExpiresByType image/vnd.microsoft.icon              "access plus 1 week"
    ExpiresByType image/x-icon                          "access plus 1 week"

  # HTML

    ExpiresByType text/html                             "access plus 0 seconds"


  # JavaScript

    ExpiresByType application/javascript                "access plus 1 year"
    ExpiresByType application/x-javascript              "access plus 1 year"
    ExpiresByType text/javascript                       "access plus 1 year"


  # Manifest files

    ExpiresByType application/manifest+json             "access plus 1 week"
    ExpiresByType application/x-web-app-manifest+json   "access plus 0 seconds"
    ExpiresByType text/cache-manifest                   "access plus 0 seconds"


  # Markdown

    ExpiresByType text/markdown                         "access plus 0 seconds"


  # Media files

    ExpiresByType audio/ogg                             "access plus 1 year"
    ExpiresByType image/apng                            "access plus 1 year"
    ExpiresByType image/bmp                             "access plus 1 year"
    ExpiresByType image/gif                             "access plus 1 year"
    ExpiresByType image/jpeg                            "access plus 1 year"
    ExpiresByType image/jpg                             "access plus 1 year"
    ExpiresByType image/png                             "access plus 1 year"
    ExpiresByType image/svg+xml                         "access plus 1 year"
    ExpiresByType image/webp                            "access plus 1 year"
    ExpiresByType video/mp4                             "access plus 1 year"
    ExpiresByType video/ogg                             "access plus 1 year"
    ExpiresByType video/webm                            "access plus 1 year"


  # WebAssembly

    ExpiresByType application/wasm                      "access plus 1 year"


  # Web fonts

    # Collection
    ExpiresByType font/collection                       "access plus 1 year"

    # Embedded OpenType (EOT)
    ExpiresByType application/vnd.ms-fontobject         "access plus 1 year"
    ExpiresByType font/eot                              "access plus 1 year"

    # OpenType
    ExpiresByType font/opentype                         "access plus 1 year"
    ExpiresByType font/otf                              "access plus 1 year"

    # TrueType
    ExpiresByType application/x-font-ttf                "access plus 1 year"
    ExpiresByType font/ttf                              "access plus 1 year"

    # Web Open Font Format (WOFF) 1.0
    ExpiresByType application/font-woff                 "access plus 1 year"
    ExpiresByType application/x-font-woff               "access plus 1 year"
    ExpiresByType font/woff                             "access plus 1 year"

    # Web Open Font Format (WOFF) 2.0
    ExpiresByType application/font-woff2                "access plus 1 year"
    ExpiresByType font/woff2                            "access plus 1 year"


  # Other

    ExpiresByType text/x-cross-domain-policy            "access plus 1 week"

</IfModule>




<IfModule mod_rewrite.c>
    Options +FollowSymlinks
    RewriteEngine On

    # initialization code - put only once at the beginning of .htaccess
    RewriteCond %{HTTPS} =on
    RewriteRule ^(.*)$ - [ENV=PROTO:https]
    RewriteCond %{HTTPS} !=on
    RewriteRule ^(.*)$ - [ENV=PROTO:http]

    #only redirect in production and if www prefix is uses Weiterleitungen von http auf https, wenn production
    RewriteCond %{ENV:ENVIRONMENT} production

    RewriteCond %{SERVER_PORT} !^443$
    RewriteRule ^(.*)$ https://%{HTTP_HOST}/$1 [R=301,NE,L]  #NE macht kein URLENCODE


    #Umleitung von domain.de auf www.domain.de, sub.domain.de bleibt auf sub.domain.de - Auskommentieren in der Produktionsumgebung
    RewriteCond %{ENV:ENVIRONMENT} production
    RewriteCond %{ENV:USEWWW} on
    RewriteCond %{SERVER_ADDR} !=127.0.0.1
    RewriteCond %{SERVER_ADDR} !=::1
    RewriteCond %{HTTP_HOST} ^[^\.]+\.[^\.]+$
    RewriteRule ^(.*)$ %{ENV:PROTO}://www.%{HTTP_HOST}/$1  [R=301,NE,L]  #NE macht kein URLENCODE


    # Cache-Images -----------------------------------------------------------------------------------

    # Add any directories you wish to omit from the Cache-Images process on a new lines:
    RewriteCond %{REQUEST_URI} !css
    RewriteCond %{REQUEST_URI} !/crop/
    RewriteCond %{REQUEST_URI} !/cache/img/
    RewriteCond %{REQUEST_URI} !/dist
    RewriteCond %{REQUEST_URI} !/uploads/
    RewriteCond %{REQUEST_URI} !.local

    #only images from media folder
    RewriteCond %{REQUEST_URI} media/

    # Send any GIF, JPG, or PNG request that IS NOT stored inside one of the above directories
    # to img-server.php so we can select appropriately sized versions
    RewriteRule \.(?:jpe?g|gif|png)$ img-server.php

    # END Cache-Images -------------------------------------------------------------------------------

    # Cached-Assets ----------------------------------------------------------------------------------
    RewriteCond %{REQUEST_URI} ^/dist/cache/

    # Rewrite cached assets to the original file which will be replaced
    RewriteRule ^(.+?)\-(.+?){32}(\..+?)$ $1$3 [L]

    # END Cached-Assets ------------------------------------------------------------------------------

    #Remove trailing slashes from the URL
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteRule ^(.*)\/(\?.*)?$ $1$2 [R=301,L]

    RewriteBase /
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^ index.php [L]
</IfModule>
