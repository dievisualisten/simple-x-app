# Formular

## Optionen nach dem Absenden 
- Die Seite, auf der das Formular plaziert wurde wird wieder angezeigt 
und der Headline & Content aus der Formular-Konfiguration wird angezeigt
  - Wenn Headline & Content leer sind, wird __('formular.send.content') und __('formular.send.title') angezeigt
  - Wenn es in den Texten formular.$identifier.content gibt, dann wird dieser genommen. $identifier kann in der Formular-Konfiguration gesetzt werden
  - Wenn in den Actionviews formular.$identifier als controller_action gesetzt ist, dann wird der zugeornete Artikel geladen und unter der URL angezeigt ... 
 
 - Wie erreicht man was? 
    - Nach dem Absenden will man die Variablen im Content ersetzen.
    - Nach dem Abenden will man auf eine andere Url weiterleiten (z.B. Vielen Dank Seite), die Variablen aus dem Submit werden in eine Session 
    gespeichert 
    - Nach dem Absenden will man den Content manipulieren
  

 
