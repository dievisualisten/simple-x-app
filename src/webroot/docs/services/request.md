# RequestService
Kann über folgende Funktionen aufgerufen werden, wobei die erste Variante die bevorzugte ist.
```php
request();
```

```php
use App\Library\ServiceManager;
ServiceManager::request();
```

```php
service('request');
```

## Funktionsübersicht
```php
/* @return \Cake\Http\ServerRequest */
request()->request();
```

```php
/* @return \Cake\Http\Session */
request()->session();
```

```php
/* @return ('http'|'https') */
request()->protocol($forceSecure = false);
```

```php
/* @return ('http://'|'https://') */
request()->scheme($forceSecure = false);
```

```php
/* @return ('localhost'|'www.client.de') */
request()->domain($useDev = false);
```

```php
/* @return (''|'/foo/bar.html'|'/foo/bar') */
request()->path($withExt = true);
```

```php
/* @return ('localhost/foo/bar.html') */
request()->url($useDev = false, $withExt = true);
```

```php
/* @return ['foo' => 'bar', 'test' => true] */
request()->query($override = []);
```

```php
/* @return ('foo=bar&test=true') */
request()->queryString($override = []);
```

```php
/* @return ('http://localhost') */
request()->fullDomain($forceSecure = false, $useDev = false);
```

```php
/* @return ('http://localhost/foo/bar.html') */
request()->fullPath($forceSecure = false, $useDev = false, $withExt = true);
```

```php
/* @return ('http://localhost/foo/bar.html?foo=bar&test=true') */
request()->fullUrl($forceSecure = false, $useDev = false, $withExt = true);
```

```php
/* @return ('html'|'json'|'xml'|'pdf') */
request()->ext($fallback = 'html');
```

```php
/* @return $types = ['html', 'json'] ?: (true|false) */
request()->is($types);
```

```php
/* @return ['de', 'foo', 'bar'] */
request()->segments($withLocale = false, $withExt = true);
```

```php
/* @return (0: 'de'|1: 'foo'|2: 'bar') */
request()->segment($index = 0, $fallback = null);
```

```php
/* @return $domain = 'http://localhost' ?: ('http://www.client.de') */
request()->getContextDomain($domain);
```


```php
/* @return $domain = 'http://localhost/foo/bar' ?: ('http://www.client.de') */
request()->getDomainFromPath($path, $forceSecure = false);
```
