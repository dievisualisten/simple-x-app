const fs = require('fs');
const portscanner = require('portscanner');
const gulp = require('gulp');
const sass = require('gulp-sass');
const merge = require('merge-stream');
const rename = require('gulp-rename');
const replace = require('gulp-replace');
const cleanCSS = require('gulp-clean-css');
const sourcemaps = require('gulp-sourcemaps');
const inlineSvg = require('gulp-sass-inline-svg');
const tilde = require('node-sass-tilde-importer');
const alias = require('node-sass-alias-importer');
const webpack = require('webpack-stream');
const webpackConfig = require('./gulpfile.webpack.js');
const browserSync = require('browser-sync').create();

/**
 * Get env file
 */

const env = require('dotenv').config({
    path: '../../.env'
});

/**
 * Sync with browser
 */

let portCount = 0;
let portState = 'error';

for ( null; portState === 'open'; portCount ++ ) {

    let ports = [
        3000 + portCount, 4000 + portCount
    ];

    portscanner.checkPortStatus(ports, '127.0.0.1', function (error, status) {
        portState = status;
    });
}

let proxyDomain = env.parsed.LOCAL_DOMAIN || 'localhost';

let dynamicUrl = function (req, res, match, url) {
    return '"' + url + '?v=' + Date.now() + '"';
};

let browserSyncConfig = {
    open: false,
    port: 3000 + portCount,
    ui: {
        port: 4000 + portCount
    },
    proxy: {
        target: proxyDomain,
    },
    rewriteRules: [
        {
            match: new RegExp('https\?://' + proxyDomain + '/(?=media)', 'g'),
            replace: '/'
        },
        {
            match: new RegExp('"(.*?\.(js|css))"', 'g'),
            fn: dynamicUrl
        }
    ],
};


gulp.task('refresh', function (done) {
    browserSync.reload();
    done();
});

/**
 * Loop theme folders
 * @param callback
 */


let fonts = [
    './node_modules/@fortawesome/fontawesome-free/webfonts/*',
    './node_modules/lightgallery/src/fonts/*',
];

let images = [
    './node_modules/lightgallery/src/img/*',
];

let scripts = [
    './node_modules/@kizmann/pico-js/dist/pico-js.js',
    './node_modules/@kizmann/pico-js/dist/pico-js.js.map',
    './node_modules/jquery/dist/jquery.min.js',
    './node_modules/moment/min/moment.min.js',
    './node_modules/moment/min/moment.min.js.map',
    './node_modules/gmaps/gmaps.min.js',
    './node_modules/gmaps/gmaps.min.js.map',
    './node_modules/uikit/dist/js/uikit.min.js',
    './node_modules/uikit/dist/js/uikit-core.min.js',
    './node_modules/uikit/dist/js/components/parallax.min.js',
    './node_modules/flickity/dist/flickity.pkgd.min.js',
    './node_modules/lightgallery/dist/js/lightgallery-all.min.js',
    './node_modules/lazysizes/lazysizes.min.js',
    './node_modules/lazysizes/plugins/parent-fit/ls.parent-fit.min.js',
    './node_modules/lazysizes/plugins/rias/ls.rias.min.js',
    './node_modules/lazysizes/plugins/optimumx/ls.optimumx.min.js',
    './node_modules/chosen-js/chosen.jquery.min.js',
    './node_modules/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js',
    './node_modules/bootstrap4-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
    './node_modules/@google/markerclustererplus/dist/markerclustererplus.min.js',
    './node_modules/@google/markerclustererplus/dist/markerclustererplus.min.js.map'
];

let moment = [
    './node_modules/moment/locale/*.js',
];


gulp.task('vendor-copy', function () {

    let bulk = [];

    fonts.forEach((src) => {
        bulk.push(gulp.src(src).pipe(gulp.dest('./dist/bundle/fonts')))
    });

    images.forEach((src) => {
        bulk.push(gulp.src(src).pipe(gulp.dest('./dist/bundle/img')))
    });

    scripts.forEach((src) => {
        bulk.push(gulp.src(src).pipe(gulp.dest('./dist/bundle/js')))
    });

    moment.forEach((src) => {
        bulk.push(gulp.src(src).pipe(gulp.dest('./dist/bundle/js/moment')))
    });

    return merge(bulk);
});

fs.readdirSync('./src/theme').forEach((theme) => {

    if ( theme === 'boilerplate' && theme === '.DS_Store' ) {
        return;
    }

    /**
     * Register svg to sass converter
     */
    gulp.task(`${theme}-sass:svg`, function () {

        let inlineSvgConfig = {
            destDir: `./src/theme/${theme}/sass/icon`
        };

        return gulp.src(`./src/theme/${theme}/icon/*.svg`)
            .pipe(inlineSvg(inlineSvgConfig));
    });

    gulp.task(`${theme}-sass:fix`, function () {

        return gulp.src(`./src/theme/${theme}/sass/icon/_sass-inline-svg.scss`)
            .pipe(replace('call($functionname', 'call(get-function($functionname)'))
            .pipe(gulp.dest(`./src/theme/${theme}/sass/icon/`));
    });

    gulp.task(`${theme}-sass:vendor`, function () {

        let importer = [
            tilde, alias({ '@src-sx': 'src-sx/' })
        ];

        let sassOptions = {
            includePaths: ['./node_modules'], importer
        };



        return gulp.src(`./src/theme/${theme}/sass/vendor.sass`)
            .pipe(sourcemaps.init())
           // .pipe(sassImportJson()) //TODO @eddy soll die Media, json laden, macht es aber nichct
            .pipe(sass(sassOptions))
            .pipe(rename('vendor.css'))
            .pipe(cleanCSS())
            .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest(`./dist/theme/${theme}/`));
    });

    /**
     * Register sass bundler
     */
    gulp.task(`${theme}-sass:development`, function () {

        let importer = [
            tilde, alias({ '@src-sx': 'src-sx/'})
        ];

        let sassOptions = {
            includePaths: ['./node_modules'], importer
        };

        return gulp.src(`./src/theme/${theme}/sass/index.sass`)
            .pipe(sourcemaps.init())
            .pipe(sass(sassOptions))
            .pipe(rename('style.css'))
            .pipe(cleanCSS())
            .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest(`./dist/theme/${theme}/`))
            .pipe(browserSync.stream({ match: '**/style.css' }));
    });

    /**
     * Register webpack development bundler
     */
    gulp.task(`${theme}-js:development`, () => {
        return gulp.src(`./src/theme/${theme}/js/index.js`)
            .pipe(webpack(webpackConfig('development')))
            .pipe(gulp.dest(`./dist/theme/${theme}/`));
    });

    /**
     * Register webpack production bundler
     */
    gulp.task(`${theme}-webpack:production`, () => {
        return gulp.src(`./src/theme/${theme}/js/index.js`)
            .pipe(webpack(webpackConfig('production')))
            .pipe(gulp.dest(`./dist/theme/${theme}/`));
    });

    gulp.task(`${theme}:development`, function () {

        browserSync.init(browserSyncConfig);

        gulp.watch([
            `./src/theme/${theme}/js/**/*.(js|jsx|vue)`,
        ], gulp.series(`${theme}-js:development`));

        gulp.watch([
            `./src/theme/${theme}/icon/**/*.(svg)`,
        ], gulp.series(`${theme}-sass:svg`, `${theme}-sass:fix`));

        gulp.watch([
            `./src-sx/theme/${theme}/**/*.(sass|scss)`,
            `./src/theme/${theme}/(sass|scss)/**/*.(sass|scss)`,
            `!./src/theme/${theme}/(sass|scss)/icon/**/*`
        ], gulp.series(`${theme}-sass:development`));

        gulp.watch([
            `./dist/theme/${theme}/**/*.js`, '../Templates/**/*.php'
        ], gulp.series('refresh'));

    });

    gulp.task(`watch:${theme}`, gulp.series(`${theme}-js:development`, `${theme}-sass:svg`,
        `${theme}-sass:fix`, `${theme}-sass:vendor`, `${theme}-sass:development`, `${theme}:development`));

});

gulp.task('watch', gulp.series('vendor-copy', 'watch:default'));

