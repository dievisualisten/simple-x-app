const path = require("path");
const webpack = require("webpack");
const TerserPlugin = require("terser-webpack-plugin");
const { VueLoaderPlugin } = require('vue-loader');

let config = {
    module: {
        rules: [
            {
                test: /.jsx?$/,
                include: [
                    path.resolve('src/'),
                ],
                loader: 'babel-loader',
                options: {
                    configFile: path.resolve(__dirname, 'babel.config.js')
                },
            },
            {
                test: /\.vue$/,
                include: [
                    path.resolve('src/'),
                ],
                use: ['vue-loader']
            }
        ],
    },
    resolve: {
        alias: {
            '@src': path.resolve(__dirname, 'src'),
            '@src-sx': path.resolve(__dirname, 'src-sx')
        }
    },
    externals: {
        'moment': 'moment',
        '@kizmann/pico-js': 'pi'
    },
    plugins: [
        new VueLoaderPlugin()
    ]
};

module.exports = function (source, target, mode = 'development') {

    let globalPackage = Object.assign({

        output: {
            filename: 'script.js',
        }

    }, config);

    globalPackage.mode = mode;

    if ( mode === 'development' ) {
        globalPackage.devtool = 'eval-source-map';
    }

    if ( mode === 'production' ) {
        globalPackage.devtool = 'source-map';
    }

    if ( mode === 'development' ) {
        return globalPackage;
    }

    let loaderOptions = new webpack.LoaderOptionsPlugin({
        minimize: true
    });

    globalPackage.plugins.push(loaderOptions);

    let terserOptions = {
        mangle: true
    };

    let terser = new TerserPlugin({
        terserOptions, extractComments: false,
    });

    let optimization = {
        minimize: true, minimizer: []
    };

    optimization.minimizer.push(terser);

    globalPackage.optimization = optimization;

    return globalPackage;
}
