<?php
/**
 * The Front Controller for handling every request
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.2.9
 * @license       MIT License (https://opensource.org/licenses/mit-license.php)
 */

use App\Application;
use Cake\Http\Server;

$rootDir = dirname(dirname(__DIR__));

$systemLink = $rootDir . '/src/webroot/simple-x';

if ( ! file_exists($systemLink) ) {
    symlink($rootDir . '/src-sx/webroot', $systemLink);
}

/**
 * Configure paths required to find CakePHP + general filepath constants
 */
require $rootDir . '/src-sx/Bootstrap/paths.php';

/*
 * Load application functions
 */
require $rootDir . '/src-sx/Bootstrap/functions.php';

/**
 * Test requirements
 */
require $rootDir . '/src/Bootstrap/requirements.php';

/**
 * For built-in server
 */
if ( php_sapi_name() === 'cli-server' ) {

    $_SERVER['PHP_SELF'] = '/' . basename(__FILE__);

    $url = parse_url(urldecode($_SERVER['REQUEST_URI']));
    $file = __DIR__ . $url['path'];
    if ( strpos($url['path'], '..') === false && strpos($url['path'], '.') !== false && is_file($file) ) {
        return false;
    }
}

/**
 * Load composer autoload
 */
require $rootDir . '/vendor/autoload.php';

/**
 *  Bind your application to the server
 */
$server = new Server(new Application(APP . 'config'));

/**
 * Run the request/response through the application and emit the response
 */
$server->emit($server->run());
