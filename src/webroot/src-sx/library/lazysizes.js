import { Arr, Str, Obj, Dom, Any } from "@kizmann/pico-js";

window.lazySizes.cfg = window.lazySizes.cfg || {};
window.lazySizes.cfg.rias = window.lazySizes.cfg.rias || {};

window.lazySizes.cfg.rias.widths = [
    180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 1920
];

window.lazySizes.cfg.getOptimumX = function () {

    let sourceDpr = window.devicePixelRatio || 1;

    let targetDpr = sourceDpr;

    if ( sourceDpr >= 2 ) {
        targetDpr = 2;
    }

    return Math.min(Math.round(targetDpr * 100) / 100, 2);
};

let detectSource = function () {

    let target = 'data-src';

    Arr.each(window.SX_BREAKPOINTS, (width, key) => {
        if ( Dom.find(document.body).width() > width ) {
            target = 'data-src-' + key;
        }
    });

    console.log(target, Dom.find(document.body).width());

    return target;
};

Dom.ready(() => {
    window.lazySizes.cfg.srcAttr = window.lazySizes.cfg.rias.srcAttr = detectSource();
});

document.addEventListener('lazybeforeunveil', function (event) {

    let dataSource = 'data-bg-default';

    if ( !! window.matchMedia("(orientation: portrait)").matches ) {
        dataSource = 'data-bg-portrait';
    }

    if ( !! window.matchMedia("(orientation: landscape)").matches ) {
        dataSource = 'data-bg-landscape';
    }

    let source = Dom.find(event.target).attr(dataSource);

    if ( Any.isEmpty(source) ) {
        return;
    }

    let parentWidth = Dom.find(event.target)
        .closest(':not(picture)').innerWidth();

    if ( Any.isEmpty(parentWidth) ) {
        parentWidth = event.detail.width;
    }

    let targetWidth = 0;

    window.lazySizesConfig.rias.widths.some((width) => {
        return (targetWidth = width) > parentWidth;
    });

    targetWidth = window.lazySizesConfig.getOptimumX() *
        targetWidth;

    Dom.find(event.target).css({
        'background-image': 'url(' + source.replace(/\s*w-width\s*/i, 'w-' + targetWidth) + '})'
    });
});