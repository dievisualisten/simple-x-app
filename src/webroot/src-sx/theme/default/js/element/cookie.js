import { Cookie, Dom, Element, Obj } from "@kizmann/pico-js";

Element.alias('cookie', function (el, options) {

    options = Obj.assign({
        toggle: '.submit',
    }, options);

    if ( !Cookie.get('SxAllowCookie', true) ) {
        $(el).css({ display: 'block' });
    }

    $(el).find(options.toggle).click(() => {

        // Set cookie
        Cookie.set('SxAllowCookie', true);

        // Hide element
        $(el).css({ display: 'none' });
    });

});

Dom.ready(() => {
    Element.observe('cookie');
});
