import { Any, Dom, Element, Obj } from "@kizmann/pico-js";

/**
 * @see https://bootstrap-datepicker.readthedocs.io/en/latest/options.html
 */

Element.alias('datepicker', function (el, options) {

    let moment = window.moment;

    options = Obj.assign({
        icons: {
            time: 'icon-clock',
            date: 'icon-calendar',
            up: 'icon-angle-up',
            down: 'icon-angle-down',
            previous: 'icon-angle-left',
            next: 'icon-angle-right',
            today: 'icon-check',
            clear: 'icon-trash',
            close: 'icon-times'
        },
        format: 'LLLL',
        showClear: true,
        keepInvalid: true
    }, options);

    if ( options.format !== 'LT' && !options.minDate ) {
        //options.minDate = moment();
    }

    if ( options.minDate && Any.isNumber(options.minDate) ) {
        options.minDate = moment().add(options.minDate, 'days');
    }

    if ( options.minDate && Any.isNumber(options.maxDate) ) {
        options.maxDate = moment().add(options.maxDate, 'days');
    }

    if ( options.display !== undefined ) {

        let display = options.display;

        $(el).on('dp.change', (event) => {

            if ( options.format === 'L' ) {
                $(display).val(event.date ?
                    event.date.format('YYYY-MM-DD') : '');
            }

            if ( options.format === 'LT' ) {
                $(display).val(event.date ?
                    event.date.format('HH:mm:ss') : '');
            }

            if ( options.format === 'LLLL' ) {
                $(display).val(event.date ?
                    event.date.format('YYYY-MM-DD HH:mm:ss') : '');
            }

        });

        let defaultValue = $(display).val();

        if ( defaultValue ) {
            $(el).val(moment(defaultValue).format(options.format));
        }

        delete options.display;
    }

    if ( options.focusNext !== undefined ) {

        let focusNext = options.focusNext;

        $(el).on('dp.change', (event) => {

            let $focusNext = $(focusNext)
                .data('DateTimePicker');

            if ( !$focusNext || !event.date ) {
                return;
            }

            let minDate = event.date.add(1, 'day');

            if ( minDate.isAfter($focusNext.date()) ) {
                $focusNext.date(minDate);
            }

            $focusNext.minDate(minDate).show();
        });

        delete options.focusNext;
    }

    $(el).datetimepicker(options);
    $(el).trigger('dp.init');
});

Dom.ready(() => {
    Element.observe('datepicker');
});
