import { Any, Dom, Element, Obj } from "@kizmann/pico-js";

/**
 * @see https://bootstrap-datepicker.readthedocs.io/en/latest/options.html
 */

Element.alias('form-visibility', function (el, options) {

    let _options = Obj.assign({
        name: '', comparator: '==', value: ''
    }, options);

    if ( Any.isString(_options.value) || Any.isNull(_options.value) ) {
        _options.value = `"${_options.value}"`;
    }

    let callback = () => {

        let value = $(`#${_options.name}`).val();

        if ( Any.isString(value) || Any.isNull(value) ) {
            value = `"${value}"`;
        }

        if ( eval(`${value} ${_options.comparator} ${_options.value}`) ) {
            return $(el).addClass('visible');
        }

        $(el).removeClass('visible');
    };

    $(`#${_options.name}`).change(callback).change();

});

Dom.ready(() => {
    Element.observe('form-visibility');
});
