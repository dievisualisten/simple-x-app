import { Dom, Element, Obj } from "@kizmann/pico-js";

/**
 * @see https://sachinchoolur.github.io/lightGallery/docs/
 */

Element.alias('light-gallery', function (el, options) {

    options = Obj.assign({
        selector: '.gallery__item',
        autoplayFirstVideo:true,
        youtubePlayerParams: {
            modestbranding: 1,
            showinfo: 0,
            rel: 0,
            controls: 0,
            autoplay:1
        },
        vimeoPlayerParams: {
            byline : 0,
            portrait : 0
        }
    }, options);

    $(el).lightGallery(options);
});

Dom.ready(() => {
    Element.observe('light-gallery');
});
