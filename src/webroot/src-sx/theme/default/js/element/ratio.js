import { Any, Dom, Element, Obj } from "@kizmann/pico-js";

Element.alias('ratio', function (el, options) {

    options = Obj.assign({
        ratio: 1,
        portraitClass: 'portrait',
        landscapeClass: 'landscape',
    }, options);

    let detectOrientation = () => {

        let width = $(el).parent().width();
        let height = $(el).parent().height();

        if ( width * parseFloat(options.ratio) >= height ) {
            $(el).addClass(options.landscapeClass);
            $(el).removeClass(options.portraitClass);
        }

        if ( width * parseFloat(options.ratio) <= height ) {
            $(el).addClass(options.portraitClass);
            $(el).removeClass(options.landscapeClass);
        }

    }

    $(window).resize(Any.debounce(detectOrientation, 150));

    detectOrientation();
});

Dom.ready(() => {
    Element.observe('ratio');
});
