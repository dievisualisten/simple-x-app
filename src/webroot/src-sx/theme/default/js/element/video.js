import { Element, Dom, Obj, Str, Any } from "@kizmann/pico-js";

Element.alias('video', function (el, options) {

    let parent = Dom.find(el).closest('.video');

    if ( Any.isEmpty(parent) ) {
        return;
    }

    Dom.find(el).on('play', () => {
        Any.delay(() => Dom.find(parent).addClass('ready'), options.delay);
    });

});

Dom.ready(() => {
    Element.observe('video');
});

