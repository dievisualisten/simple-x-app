import { Element, Dom, Obj, Str, Any } from "@kizmann/pico-js";

/**
 * @see https://github.com/vimeo/player.js
 */

Element.alias('vimeo', function (el, options) {

    let parent = Dom.find(el).closest('.video');

    if ( Any.isEmpty(parent) ) {
        return;
    }

    let params = {
        dnt: 1,
        background: 1,
    };

    options = Obj.assign({
        id: null, delay: 100, params
    }, options);

    let src = `https://player.vimeo.com/video/${options.id}?` +
        Str.params(options.params);

    let iframe = Dom.make('iframe', {
        src: src
    }).get(0);

    let player = new Vimeo.Player(iframe);

    player.on('play', function() {
        Any.delay(() => Dom.find(parent).addClass('ready'), options.delay)
    });

    Dom.find(el).replace(iframe);
});

Dom.required(() => {
    Element.observe('vimeo');
}, ['Vimeo']);

