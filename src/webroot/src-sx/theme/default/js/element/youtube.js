import { Element, Dom, Obj, Any } from "@kizmann/pico-js";

/**
 * @see https://github.com/vimeo/player.js
 */

Element.alias('youtube', function (el, options) {

    let parent = Dom.find(el).closest('.video');

    if ( Any.isEmpty(parent) ) {
        return;
    }

    let params = {
        autoplay: 1,
        loop: 1,
        controls: 0,
        showinfo: 0,
        mute: 1,
    };

    options = Obj.assign({
        id: null, delay: 100, params
    }, options);

    let events = {};

    events.onReady = (event) => {
        if ( options.params.autoplay ) {
            event.target.playVideo()
        }
    };

    events.onStateChange = (event) => {
        if ( event.data == YT.PlayerState.PLAYING ) {
            Any.delay(() => Dom.find(parent).addClass('ready'), options.delay);
        }
    };

    let player = new YT.Player(el, Obj.assign({
        host: 'https://www.youtube-nocookie.com', videoId: options.id, playerVars: options.params, events
    }, options.params));

});

Dom.required(() => {
    Element.observe('youtube');
}, ['YT.Player']);