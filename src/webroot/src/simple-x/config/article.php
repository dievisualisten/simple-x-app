<script>

    /**
     * Custom tabs for articles edit header tabs
     */

    pi.Dom.ready(function () {
        sx.Form.add('sx-article-default', {

            // <NTabsItem:0>
            'NTabsItem:0': {
                vIf: function () {
                    return !!sx.Config.article('types.faq');
                },
                $props: {
                    name: 'faq',
                    sort: 300,
                    label: pi.Locale.trans('Akkordeon'),
                    icon: 'fa fa-question-circle',
                    relative: true,
                    keep: true,
                },
                content: {
                    'SxElementPanel:0': {
                        $props: {
                            type: 'faq',
                            prop: 'elements.faqs'
                        }
                    }
                }
            }
        });
    });
</script>
