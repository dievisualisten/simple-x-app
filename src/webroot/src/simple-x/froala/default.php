<script>
    window.froalaDefault = {

        key: '<?=FROALA_KEY?>',

        zIndex: 20000,

        iframe: true,
        attribution: false,

        pluginsEnabled: [
            'align',
            'charCounter',
            'codeBeautifier',
            'codeView',
            'colors',
            'draggable',
            'embedly',
            'emoticons',
            'entities',
            'file',
            'fontAwesome',
            'fontFamily',
            'fontSize',
            'fullscreen',
            'image',
            'imageTUI',
            'imageManager',
            'inlineStyle',
            'inlineClass',
            'lineBreaker',
            'lineHeight',
            'link',
            'lists',
            'paragraphFormat',
            'paragraphStyle',
            'quote',
            'save',
            'table',
            'url',
            'video',
            'wordPaste'
        ],

        htmlAllowedStyleProps: [
            //
        ],

        htmlAllowedEmptyTags: [
            'a', 'iframe', 'object', 'video', 'style', 'script', 'i', 'em', 'span'
        ],

        iframeStyleFiles: [
            '/src/simple-x/froala/froala.css'
        ],

        pasteAllowedStyleProps: [
            'text-align', 'vertical-align'
        ],

        pasteDeniedAttrs: [
            'height', 'width', 'class', 'id', 'border', 'cellpadding', 'cellspacing'
        ], //werden beim Insert gelöscht

        wordDeniedAttrs: [
            'style', 'height', 'width', 'class', 'id', 'border', 'cellpadding', 'cellspacing'
        ],

        placeholderText: '',
        entities: '&quot;',
        language: 'de',
        lineBreakerTags: [],

        //CONFIG FÜR einzelne Tools im Menü

        paragraphFormat: {
            N: 'Copytext / Normal',
            H2: 'Überschrift 2',
            H3: 'Überschrift 3',
            H4: 'Überschrift 4',
            H5: 'Überschrift 5',
            H6: 'Überschrift 6',
            small: 'Klein'
            /*H5: 'Überschrift 5'*/
        },

        //hier um Klassen ergänzen, wenn gebraucht und in der Toolbar einkommentieren
        paragraphStyles: {
            'class': 'label'
        },

        //## Link in der Toolbar
        linkInsertButtons: [
            'linkBack'
        ],
        linkAttributes: {
            title: 'Title',
            rel: 'Rel',
            class: 'Class'
        },

        linkConvertEmailAddress: true,

        //##Link im Text Toolbar
        //linkAutoPrefix: 'https://',

        //hier um Klassen ergänzen, wenn gebraucht
        linkStyles: {
            'class': 'label'
        },

        //linkStyle einkommentieren wenn gebraucht
        linkEditButtons: [
            'linkOpen', /*'linkStyle',*/ 'linkEdit', 'linkRemove'
        ],

        //##TABLE

        tableStyles: {
            priceTable: 'Preistabelle'
            /*class2: 'Class 2'*/
        },

        tableEditButtons: [
            'tableStyle', 'tableHeader', 'tableRemove', '-', 'tableRows', 'tableColumns', 'tableCells', /*'tableCellBackground',*/ '-', 'tableCellVerticalAlign', 'tableCellHorizontalAlign' /*', tableCellStyle'*/
        ],
        tableResizer: false,
        tableInsertHelper: false,

        toolbarButtons: [
            'paragraphFormat',
            //'paragraphStyle',  // <- hier einkommentieren, wenn gebraucht
            '|',
            //,
            //'print',
            'bold',
            'italic',
            //'underline',
            'strikeThrough',
            'subscript',
            'superscript',
            //'fontFamily',
            //'fontSize',
            '|',
            //'specialCharacters',
            //'color',
            //'emoticons',
            //'inlineStyle',
            'insertHR',
            //'align',
            'formatOL',
            'formatUL',
            //'outdent',
            //'indent',
            // 'quote',
            '|',
            'insertLink',
            //'insertImage',
            //'insertVideo',
            //'insertFile',
            'insertTable',
            'undo',
            'redo',
            'clearFormatting',
            'selectAll',
            'html'
        ]

    };

    /* Fix for rewriting : to %3A
    * https://github.com/froala/wysiwyg-editor/issues/1235
    */
    var original_helpers = FroalaEditor.MODULES.helpers;

    FroalaEditor.MODULES.helpers = function (editor) {
        var helpers = original_helpers(editor);

        var isURL = helpers.isURL();

        if ( isURL === false ) {
            return helpers;
        }

        // This is the original sanitizer.
        helpers.sanitizeURL = function (url) {
            if ( /^(https?:|ftps?:|)\/\//i.test(url) ) {
                if ( isURL && !isURL(url) && !isURL('http:' + url) ) {
                    return '';
                }
            } else {
                url = encodeURIComponent(url)
                    .replace(/%23/g, '#')
                    .replace(/%2F/g, '/')
                    .replace(/%25/g, '%')
                    .replace(/mailto%3A/gi, 'mailto:')
                    .replace(/file%3A/gi, 'file:')
                    .replace(/sms%3A/gi, 'sms:')
                    .replace(/tel%3A/gi, 'tel:')
                    .replace(/notes%3A/gi, 'notes:')
                    .replace(/data%3Aimage/gi, 'data:image')
                    .replace(/blob%3A/gi, 'blob:')
                    .replace(/webkit-fake-url%3A/gi, 'webkit-fake-url:')
                    .replace(/%3F/g, '?')
                    .replace(/%3D/g, '=')
                    .replace(/%26/g, '&')
                    .replace(/&amp;/g, '&')
                    .replace(/%2C/g, ',')
                    .replace(/%3B/g, ';')
                    .replace(/%2B/g, '+')
                    .replace(/%40/g, '@')
                    .replace(/%5B/g, '[')
                    .replace(/%5D/g, ']')
                    .replace(/%7B/g, '{')
                    .replace(/%7D/g, '}')
                    .replace(/%3A/g, ':');

            }

            return url;
        }

        return helpers;
    }


</script>
