import { Dom } from '@kizmann/pico-js';

Dom.ready(() => {
    UIkit.scrollspy('.animate', { cls: 'uk-animation-slide-bottom-small', delay: 150 });
});